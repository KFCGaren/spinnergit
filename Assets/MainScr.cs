﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainScr : MonoBehaviour {

    public InputField cijenaIF, kolicinaIF, PV, TJ, LK;
    public Text zaradaTxt, prodajeTxt, podjela, podjelaProsla;
    float cijena = 30f, zarada;
    int kolicina = 1, prodaje;
    string[] ljudi = { "Patrik Vuković","Tin Jurić","Luka Kušec" };
    float[] postotci = { 0.45f, 0.45f, 0.1f };
    public GameObject panel;

    void Start()
    {
        Azuriraj();
        kolicinaIF.text = "1";
        cijenaIF.text = cijena.ToString();
        postotci.SetValue(PlayerPrefs.GetFloat("PV"), 0);
        postotci.SetValue(PlayerPrefs.GetFloat("TJ"), 1);
        postotci.SetValue(PlayerPrefs.GetFloat("LK"), 2);
    }

    public void Izracun()
    {
        cijena = int.Parse(cijenaIF.text);
        PlayerPrefs.SetFloat("cijena", cijena);
        kolicina = int.Parse(kolicinaIF.text);
        zarada += cijena * kolicina;
        PlayerPrefs.SetFloat("zarada", zarada);
        PlayerPrefs.SetInt("prodaje", prodaje + kolicina);
        podjelaProsla.text = "Podjela posljednje zarade:\n\n";
        for (int i = 0; i < ljudi.Length; i++)
        {
            podjelaProsla.text += ljudi[i] + ": " + postotci[i] * cijena * kolicina + " kn\n";
        }
        podjelaProsla.text += "\n Ukupno: " + cijena*kolicina + " kn";
        Azuriraj();
    }

    void Azuriraj()
    {
        cijena = PlayerPrefs.GetFloat("cijena");
        zarada = PlayerPrefs.GetFloat("zarada");
        prodaje = PlayerPrefs.GetInt("prodaje");
        zaradaTxt.text = "Dosad zarađeno: " + zarada.ToString() + " kn";
        prodajeTxt.text = "Dosad prodano: " + prodaje.ToString() + " komada";
        podjela.text = "Podjela zarade:\n\n";
        for (int i = 0; i < ljudi.Length; i++)
        {
            podjela.text += ljudi[i] + ": " + postotci[i] * zarada + " kn\n";
        }
        podjela.text += "\nUkupno: " + zarada + " kn";
    }


    public void Reset()
    {
        PlayerPrefs.SetInt("prodaje", 0);
        PlayerPrefs.SetFloat("zarada", 0);
        PlayerPrefs.SetFloat("PV", 0.45f);
        PlayerPrefs.SetFloat("TJ", 0.45f);
        PlayerPrefs.SetFloat("LK", 0.1f);
        postotci.SetValue(PlayerPrefs.GetFloat("PV"), 0);
        postotci.SetValue(PlayerPrefs.GetFloat("TJ"), 1);
        postotci.SetValue(PlayerPrefs.GetFloat("LK"), 2);
        Azuriraj();
    }


    public void Postavke()
    {
        panel.SetActive(true);
        PV.text = postotci[0] * 100 + "";
        TJ.text = postotci[1] * 100 + "";
        LK.text = postotci[2] * 100 + "";
    }

    public void PostavkeAzuriraj()
    {
        postotci[0] = int.Parse(PV.text)/100f;
        PlayerPrefs.SetFloat("PV", postotci[0]);
        postotci[1] = int.Parse(TJ.text)/100f;
        PlayerPrefs.SetFloat("TJ", postotci[1]);
        postotci[2] = int.Parse(LK.text)/100f;
        PlayerPrefs.SetFloat("LK", postotci[2]);
    }

    public void Back()
    {
        panel.SetActive(false);
    }
}